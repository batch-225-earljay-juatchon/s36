//  Setup the dependencies

const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv").config();


const taskRoute = require("./routes/taskRoute")

//  Server setup
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Database Connection
//  Connect to MongoDB atlas

mongoose.connect(`mongodb+srv://earljay0606:${process.env.PASSWORD}@cluster0.pfnycvo.mongodb.net/MRC?retryWrites=true&w=majority`,
{
	useNewUrlParser : true,
	useUnifiedTopology : true
}

	);

//  Set up notification for connection success or failure
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.on('open', () => console.log('Connected to MongoDB'));
 
//  Add the task routes
//  Allows all the task routes created in the "taskRoute.js" file to use "./tasks" route

app.use("/tasks", taskRoute);


//  Port Connect
//  Server listening
app.listen(port, () => console.log(`Now listening to port ${port}`));



 // Activity ===================================

